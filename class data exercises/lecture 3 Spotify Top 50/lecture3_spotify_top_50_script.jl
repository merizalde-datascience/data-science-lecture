#Author: Jaime Lopez-Merizalde, Ph.D.
#Date: 10 12 2020
#UpD: 16 12 2020
#Descri: top 50 spotify songs lecture 3 of visualizations
#Notes:
# lecture3_spotify_top_50_script.jl

#---Libraries
using DelimitedFiles, Printf
using DataFrames, CSV, Query
using StatsPlots, Gadfly
using Statistics, StatsBase

df = DataFrames

#---Directories and NAmes
pwdd = pwd()
spotify_top50_name = "top50.csv"
spotify_auxillary_name = "spotify_aux.csv"

#----Data Load
file = readdlm(spotify_top50_name, ',')
file2 = readdlm(spotify_auxillary_name,',')


#----Data Manipulation Wihtout DataFrames/Pandas

#Store file as Dictionary (key-value pair)
attributes = file[1,:]
dataSet = Array{Dict,1}(UndefInitializer(), size(file,1)-1)

for line in collect(1:size(file,1)-1)
    dataSet[line] = Dict(zip(attributes,file[line+1,:]))
end

# #Q2: Write code to print all songs (Artist and Track
# #name) that are longer than 4 minutes (240 seconds):
# for song in dataSet
#     if song["Length"] > 240.0
#         println(" $(song["ArtistName"]) - $(song["TrackName"]) is $(song["Length"]) seconds long.")
#     end
#
# end


# #Q3: Write code to print the most popular song (artist and
# #track) – if ties, show all ties.
# MostPopular = Array{String,1}(UndefInitializer(),1)
# CurrentScore = 0
# for song in dataSet
#     global CurrentScore
#     if song["Popularity"] > CurrentScore
#         CurrentScore = song["Popularity"]
#         MostPopular[1] = "$(song["ArtistName"]) - $(song["TrackName"]) - popularity $(song["Popularity"])"
#     elseif song["Popularity"] == CurrentScore
#         push!(MostPopular, "$(song["ArtistName"]) - $(song["TrackName"]) - popularity $(song["Popularity"])")
#     end
# end
# println(MostPopular)

# # Q4: Write code to print the songs (and their attributes),
# # if we sorted by their popularity (highest scoring ones first).
# sortedSongsByPopularity = Array{Any,2}(UndefInitializer(), size(dataSet,1), 2)
# idx_song = 1
# for song in dataSet
#     global idx_song
#     #place each song as dictionary in a this Array
#     sortedSongsByPopularity[idx_song,:]= [song,song["Popularity"]]
#     idx_song += 1
# end
# idxSortedSongs = sortperm(sortedSongsByPopularity[:,2], rev = true)
# #now to the printing
# println("\n Sorted Songs By Popularity \n")
# for idxSong in idxSortedSongs
#     for attribute in attributes
#         print("$attribute: $(dataSet[idxSong][attribute]) ")
#     end
#     println()
# end

#---- Using DataFrames

#Reading in a CSV and casting to a dataFrame
dataFrame_top50 = CSV.File(spotify_top50_name) |> DataFrame
dataFrame_aux   = CSV.File(spotify_auxillary_name) |> DataFrame

#reading out the first 6 rows in the dataframe
first(dataFrame_top50,6)
#reading out the last 6 rows in the dataframe
last(dataFrame_top50,6)

#attributes,
names(dataFrame_top50)

length(names(dataFrame_top50))

#descriptive statistics
describe(dataFrame_top50)

#attributes and attribute dataType
hcat(propertynames(dataFrame_top50),[ eltype(col) for col = eachcol(dataFrame_top50)])

#calling columns in a dataFrame_top50
dataFrame_top50.Length

#OR like this
dataFrame_top50[:,:Length]

#ArgMin or ArgMax
argmin(dataFrame_top50.Length)
argmax(dataFrame_top50.Length)

dataFrame_top50.Length .> 240

dataFrame_top50[1,:]

dataFrame_top50[[1,2],:]

dataFrame_top50[(dataFrame_top50[:,:Length].>240),:]

dataFrame_top50[(dataFrame_top50[:, :Length] .> 240), :][:,[:ArtistName, :TrackName, :Length]]

# show(dataFrame_top50[argmax(dataFrame_top50[:,:Popularity]),:];allrows=true,allcols=true)

#Q4: Write code to print the songs (and their attributes),
# if we sorted by their popularity (highest scoring ones first).
dataFrame_top50[:, :Popularity]

# show(sort!(dataFrame_top50, [:Popularity],rev = true);allcols=true)

# query3 = @from i in dataFrame_top50 begin
#             @where i.Length >= 240
#             @select i.TrackName
#             @collect
#         end
dataFrame_top50[end,:]

#Finding missing values in column 3
findall(x -> ismissing(x), dataFrame_top50[:,3])

ArrayMissingValues = Array{Array{Int16,2},1}(UndefInitializer(),1)
#remove first undefined value
deleteat!(ArrayMissingValues,1)
# println(ArrayMissingValues)

#GROUPING Data
grouped_top50 = groupby(dataFrame_top50, :Genre)

# for key in grouped_top50
#     print("Genre: ", key[1,:Genre], " (",
#     length(grouped_top50[grouped_top50.keymap[("$(key[1,:Genre])",)]].Length),
#     " items):", key, "\n\n")
# end

innerjoin(dataFrame_aux, dataFrame_top50, on = :TrackName, makeunique = true)
# show(innerjoin(dataFrame_aux, dataFrame_top50, on = :TrackName, makeunique = true),allcols=true)

innerjoin(dataFrame_aux[:,[:TrackName,:ExplicitLanguage]], dataFrame_top50, on = :TrackName)
# show(innerjoin(dataFrame_aux[:,[:TrackName,:ExplicitLanguage]], dataFrame_top50, on = :TrackName), allcols=true)


#---- Plotting DataFrames
#pandas: scatter_plot = top50.plot.scatter(x='Danceability', y='Popularity', c='DarkBlue')

#julia:

@df dataFrame_top50 scatter(
    :Danceability,
    :Popularity,
    title = "Popularity vs Danceability",
    xlabel = "Danceability",
    ylabel = "Popularity",
    label = "Danceability",
    m = (0.9, [:o], 12),
    bg = RGB(0.9, 0.9, 0.9)
)


#---- Problems

# Problem 1: P1. Print the shortest song (all features):
# println(dataFrame_top50[argmin(dataFrame_top50.Length),:])

# Problem 2: P2 Print the 5 shortest songs (all features):
# println(sort(dataFrame_top50,[:Length])[1:5,:])

# P3. What is the average length of the 5 shortest songs?
# mean(sort(dataFrame_top50,[:Length])[1:5,:].Length)

#P4. Write a function that accepts a DataFrame as an input
# and returns True if there exists any null values in the DataSet.
#Otherwise, returns False. Pass top50 to the function in order to test it.

# function dataFrameMissingChecker(dataFrame::DataFrame)
#
#     ArrayMissingValues = Array{Array{Int16,2},1}(UndefInitializer(),1)
#     for idx_row_element in 1:length(eachrow(dataFrame))
#
#         for idx in findall(x -> ismissing(x), dataFrame[idx_row_element,:])
#             push!(ArrayMissingValues,[idx_song findall(x-> x==idx, propertynames(dataFrame))[1]])
#         end
#     end
#     #remove first undefined value
#     deleteat!(ArrayMissingValues,1)
#     println(ArrayMissingValues)
#     if length(ArrayMissingValues) > 0
#         thisBoolean = true
#     else
#         thisBoolean = false
#     end
#
#     return thisBoolean
# end

# dataFrameMissingChecker(dataFrame_top50)

# P5. How many distinct genres are present in the top 50 songs?

# println("Unique genres: $(length(unique(dataFrame_top50[:,:Genre])))")

# P6. Print the songs that have a Danceability score above 80
# and a popularity above 86. HINT: you can combine conditional statements
# with the & operator, and each item must be surrounded with ( ) brackets.

# bitIndexArray = (dataFrame_top50[:,:Danceability].>80).*(dataFrame_top50[:,:Popularity].>86)
# println(dataFrame_top50[bitIndexArray,:])

#  P7. Print the songs that are faster than
# the average Top 50 and more popular than the average Top 50?

# bitIndexArray =(dataFrame_top50[:,:BeatsPerMinute].<mean(dataFrame_top50[:,:BeatsPerMinute])).*(dataFrame_top50[:,:Popularity].>mean(dataFrame_top50[:,:Popularity]))
# println(dataFrame_top50[bitIndexArray,:])

# P8. Plot a histogram of the Genre counts
# (x-axis is the Genres, y-axis is the # of songs with that Genre)

top50_genre_countmap = countmap(dataFrame_top50[:,:Genre])
bar(collect(keys(top50_genre_countmap)), collect(values(top50_genre_countmap))
,bar_width=0.45,rotation=45.0, label="Genre",xticks=:all)
